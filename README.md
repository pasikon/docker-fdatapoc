## Preface

This project is containg PoC for our SaaS. Its is just a part of complete Fast Data architecture that is composed together in Docker containers. There is also some proofing functionality which is involving Tensorflow model serving (YES! Seedlings images recognition) via Flink where model & pictures are stored in HDFS and integration points are Kafka topics.

Composing stuff with Docker made all infrastucture startup as simple as invoking one shell command.

## Infrastructure

Currently architecture consists of:

1. Hadoop (namenode, datanode, HUE file browser, Postgres metastore)
2. Zookeeper
3. Kafka
4. Spark 2.3 (master & 1 worker standalone cluster) (TODO: configure Cassandra connector, test Spark SQL, set up HDFS history Server)
5. Flink 1.4.2 (jobmanager & taskmanager standalone cluster)
6. Cassandra
7. Jupyter Lab to play with Spark & other stuff (TODO: volume for notebooks, confugure PySpark)

8. TODO: some web console/monitoring for Kafka
9. Elasticsearch & Kibana

### System setup

Elastic Search uses `mmapfs` directory by default to store its indices. The default operating system limits on mmap counts is likely to be too low, which may result in out of memory exceptions.

```bash
sysctl -w vm.max_map_count=262144
```
To set this value permanently, update the `vm.max_map_count` setting in `/etc/sysctl.conf`. To verify after rebooting, run `sysctl vm.max_map_count`.

### Running & accessing all the stack

First you need to create a separate docker network named **lizard2** for all the stuff!

```bash
docker network create --driver=bridge --subnet=192.168.0.0/16 lizard2
```

Then run all the stuff as simple as:

```bash
cd docker-fdatapoc
docker-compose up
```

Let it download all stuff (should take a LOONG time, few GB I think).

After it all starts you should be running following containers:

```bash
dockerfdatapoc_jupyter_1                  
dockerfdatapoc_spark-worker-1_1           
dockerfdatapoc_spark-master_1             
dockerfdatapoc_kafka_1                    
dockerfdatapoc_flink-taskm_1              
dockerfdatapoc_flink-jobm_1               
dockerfdatapoc_hive-server_1              
dockerfdatapoc_presto-coordinator_1       
dockerfdatapoc_datanode_1                 
dockerfdatapoc_hive-metastore_1           
dockerfdatapoc_hue_1                      
dockerfdatapoc_cassandra_1                
dockerfdatapoc_namenode_1                 
dockerfdatapoc_hive-metastore-postgresql_1
dockerfdatapoc_zookeeper_1
```

### Accessing some services for fun

#### HUE file browser for HDFS

<http://localhost:8088/home>

It may prompt for user creation, type admin/admin or whatever

#### Flink web console

<http://localhost:8081>

#### Hadoop web consoles

Namenode: <http://localhost:50070>
Datanode: <http://localhost:50075>

#### Jupyter Spark Notebook

<http://localhost:8888>

## Interacting with the stack

#### Copying files to HDFS

Because all is dockerized in separate network we interact with the stack from ad-hoc Docker containers, EASY!

Because some Flink job I developed is serving image recognizing model lets copy the Tensorflow model & pictures to HDFS, let it be a refenernce how to interact with Hadoop here.

You can use **Hue** <http://localhost:8088/home> to create `/user/flink/tf_models/` directory structure. You can also do it via command line:

```bash
docker run -it --rm --env-file=./hadoop-hive.env --volume $PWD/tf_models:/data --network lizard2 bde2020/hadoop-namenode:2.0.0-hadoop2.7.4-java8 hadoop fs -mkdir /user/flink/

docker run -it --rm --env-file=./hadoop-hive.env --volume $PWD/tf_models:/data --network lizard2 bde2020/hadoop-namenode:2.0.0-hadoop2.7.4-java8 hadoop fs -mkdir /user/flink/tf_models/
```

Please execute following commands from project root dir as they use `$PWD`

Copy Tensorflow model to HDFS:

```bash
docker run -it --rm --env-file=./hadoop-hive.env --volume $PWD/tf_models:/data --network lizard2 bde2020/hadoop-namenode:2.0.0-hadoop2.7.4-java8 hadoop fs -copyFromLocal /data/frozen_SeedlingsRecognize.pb /user/flink/tf_models/
```

Copy Seedlings Images to HDFS:

```bash
docker run -it --rm --env-file=./hadoop-hive.env --volume $PWD/seedling_pics:/data --network lizard2 bde2020/hadoop-namenode:2.0.0-hadoop2.7.4-java8 hadoop fs -copyFromLocal /data/ /user/flink/seedling_pics
```


#### Interacting with Kafka

Its the best to open Kafka producer and consumer in separate terminal tabs.

Here one Kafka topic is where you put file name that is seedling picture that is to be recognized, second one is where plan name gonna be put.

Run Kafka producer (for file names containing seedling pics):

NOTE: existing Flink example job is listening on `img_file_hdfs` and producting to `img_class_softmax` topic so here you got proper commands :)

```bash
docker run -it --rm --network lizard2 -e KAFKA_ZOOKEEPER_CONNECT="zookeeper:2181" bitnami/kafka:0.11.0-1-r1 kafka-console-producer.sh --broker-list kafka:9092 --topic img_file_hdfs
```

Create topic for plant names:

```bash
docker run -it --rm --network lizard2 -e KAFKA_ZOOKEEPER_CONNECT="zookeeper:2181" bitnami/kafka:0.11.0-1-r1 kafka-topics.sh --create --zookeeper zookeeper:2181 --replication-factor 1 --partitions 1 --topic img_class_softmax
```

Run Kafka consumer for plant names:

```bash
docker run -it --rm --network lizard2 -e KAFKA_ZOOKEEPER_CONNECT="zookeeper:2181" bitnami/kafka:0.11.0-1-r1 kafka-console-consumer.sh --bootstrap-server kafka:9092 --topic img_class_softmax
```

List topics if you want:

```bash
docker run -it --rm --network lizard2 -e KAFKA_ZOOKEEPER_CONNECT="zookeeper:2181" bitnami/kafka:0.11.0-1-r1 kafka-topics.sh --list --zookeeper zookeeper:2181
```

#### Using Jupyter

##### 1. Log in
First we need to get token that is required to authenticate into Jupyter:

```bash
docker logs backend-stack-docker_jupyter_1
```

You should see something like this:

```
Copy/paste this URL into your browser when you connect for the first time,
    to login with a token:
        http://localhost:8888/?token=4e0593ba9e53aef9848a5becb6fcbc12eaa620c6f777e13f
```

Copy line conitaing `localhost:8888` & put it into browser.

##### 2. Run some code

Create `Apache Toree` Scala Notebook by clicking on its icon.

Get Spark Context conf:

```bash
sc.getConf.getAll
```

Hadoop configuration:

```bash
sc.hadoopConfiguration
```

List files on HDFS (if you created admin user you can upload some file using Hue):

```bash
val files = org.apache.hadoop.fs.FileSystem.get(sc.hadoopConfiguration).listFiles(new org.apache.hadoop.fs.Path("hdfs:///user/admin"), true)
files.next
```

Some computation :)

```bash
sc.parallelize(1 to 1000).reduce(_ + _)
```

## Streaming Examples

**All job examples I did so far are present here: <https://github.com/pasikon/flink-seedling-processor>**

Please checkout the repository & issue `sbt clean assembly`. JAR is to be found in `target/scala_2_11/`. JAR size is insane because one of the job is actually manipulating images so it has native dependencies for every platform (ppc, x86, arm, etc.) I need to investigate how to limit the size.

All jobs mentioned below are packaged in same JAR you have just built.

Dig into the code to analyse whats going on there :)

#### Uploading JAR to Flink

1. Go to Flink console <http://localhost:8081>
2. Click `Submit new job` on the left
3. Click `Add new +`
4. Choose JAR, click green `Upload`
5. Click checkbox to the left of JAR name to specify JOB Entry class (see streaming examples below), set parallelism to 1 or 2, then `Submit`

### Simple kafka->kafka job

1. Entry JOB class = `org.michal.FromKafkaToKafka`
2. In separate shells type to have Kafka topic producer & consumer:

```bash
docker run -it --rm --network lizard2 -e KAFKA_ZOOKEEPER_CONNECT="zookeeper:2181" bitnami/kafka:0.11.0-1-r1 kafka-console-producer.sh --broker-list kafka:9092 --topic k2k_i
docker run -it --rm --network lizard2 -e KAFKA_ZOOKEEPER_CONNECT="zookeeper:2181" bitnami/kafka:0.11.0-1-r1 kafka-console-consumer.sh --bootstrap-server kafka:9092 --topic k2k_o
```

Then type something in the producer to see output in `k2k_o` topic as string length

### Seedling Image recognition Stream with Tensorflow model

1. Copy all the needed files to HDFS

Copy Tensorflow model to HDFS:

```bash
docker run -it --rm --env-file=./hadoop-hive.env --volume $PWD/tf_models:/data --network lizard2 bde2020/hadoop-namenode:2.0.0-hadoop2.7.4-java8 hadoop fs -mkdir /user/flink/

docker run -it --rm --env-file=./hadoop-hive.env --volume $PWD/tf_models:/data --network lizard2 bde2020/hadoop-namenode:2.0.0-hadoop2.7.4-java8 hadoop fs -mkdir /user/flink/tf_models/

docker run -it --rm --env-file=./hadoop-hive.env --volume $PWD/tf_models:/data --network lizard2 bde2020/hadoop-namenode:2.0.0-hadoop2.7.4-java8 hadoop fs -copyFromLocal /data/frozen_SeedlingsRecognize.pb /user/flink/tf_models/
```

Copy Seedlings Images to HDFS:

```bash
docker run -it --rm --env-file=./hadoop-hive.env --volume $PWD/seedling_pics:/data --network lizard2 bde2020/hadoop-namenode:2.0.0-hadoop2.7.4-java8 hadoop fs -copyFromLocal /data/ /user/flink/seedling_pics
```

2. Entry JOB class (for Flink job deployment) = `org.michal.imgproc.SeedlingClassifyHDFS`
3. In separate shells type to have Kafka topic producer & consumer:

Input image to recognize (its stored in HDFS!):

```bash
docker run -it --rm --network lizard2 -e KAFKA_ZOOKEEPER_CONNECT="zookeeper:2181" bitnami/kafka:0.11.0-1-r1 kafka-console-producer.sh --broker-list kafka:9092 --topic img_file_hdfs
```

Reading result from Kafka:

```bash
docker run -it --rm --network lizard2 -e KAFKA_ZOOKEEPER_CONNECT="zookeeper:2181" bitnami/kafka:0.11.0-1-r1 kafka-console-consumer.sh --bootstrap-server kafka:9092 --topic img_class_softmax
```
4. Observe job logs by issuing command: `docker logs -f docker-fdatapoc_flink-taskm_1`
5. Send to `img_file_hdfs` topic file name such as `0ad9e7dfb.png`. Use HUE browser to see images under `/user/flink/seedling_pics` to see which images are there for you to recognize.
6. Observe output @ `img_class_softmax` topic, should be recognized plant name. **NOTE:** if you specify non existent image default image will be used, that means you will always receive same plant name

### Deleting cluster

```bash
docker-compose rm
```

### TODO: another example
